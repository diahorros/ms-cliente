package com.msclientes.demo.RestController;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.msclientes.demo.Entidades.Cliente;

@CrossOrigin("*")
@RestController
@RequestMapping(path = "/api")


public class ClientesRestController {
	List<Cliente> lsClientes = new ArrayList<Cliente>() ;
	//Listar 
	@RequestMapping("/listarclientes")
	public Cliente getClientes() {
		
		Cliente objCliente = new Cliente("1104752140", "Jimmy Andress", "Portilla Guaman", "2615558","japortilla@gmail.com");
		return objCliente;
	}


	
	// Listar 
		@RequestMapping("/listarclientes2")
		public List<Cliente> getClientes2() {
			//List<Cliente> lsClientes = new ArrayList<Cliente>() ;
			
			Cliente objCliente = new Cliente("1104871619", "Adrian Francisco", "Portilla Guaman", "2615551","afportilla@gmail.com");
			Cliente objCliente2 = new Cliente("1104871617", "Bryan David", "Portilla Guaman", "2615558","bdportilla@gmail.com");
			
			lsClientes.add(objCliente);
			lsClientes.add(objCliente2);
			
			return lsClientes;
		}


	// Guardar y actualizar Cliente
		@PostMapping("/guardarCliente")
		public void guardarCliente(@RequestBody Cliente cliente) {
			//aqui va el acceso a base para guardar
			//servicio.metodoguardar(cliente)
			System.out.println("Se guardo correcto");
			
			lsClientes.add(cliente);
		}

		


}
